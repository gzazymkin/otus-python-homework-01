# otus-python-homework-01

First homework (log analyzer).

### Usage example
With custom config file:

`python3 main.py --config config.json`

With default (config.json) config file:

`python3 main.py`

### Testing
`python3 -m unittest`